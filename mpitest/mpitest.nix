{ pkgs, ... }:
let
  mpitest =
    let mpitestC = pkgs.writeText "mpitest.c" (builtins.readFile ./mpitest.c);
    in pkgs.runCommand "mpitest" { } ''
      mkdir -p $out/bin
      ${pkgs.openmpi}/bin/mpicc ${mpitestC} -o $out/bin/mpitest
    '';
in { environment.systemPackages = [ mpitest ]; }
