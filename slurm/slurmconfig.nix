{ pkgs, nComputeNode }:
let mpitest = import ../mpitest/mpitest.nix;
in {
  imports = [ mpitest ];

  services.openssh.forwardX11 = false;
  services.slurm = {
    controlMachine = "server";
    nodeName = [ "computeNode[1-${nComputeNode}] CPUs=1 State=UNKNOWN" ];
    partitionName = [
      "debug Nodes=computeNode[1-${nComputeNode}] Default=YES MaxTime=INFINITE State=UP"
    ];
    extraConfig = ''
      AccountingStorageHost=dbd
      AccountingStorageType=accounting_storage/slurmdbd
      MpiDefault=pmix
    '';
  };

  environment.systemPackages =
    [ pkgs.nur.repos.kapack.melissa pkgs.nur.repos.kapack.melissa-heat-pde ];

  networking.firewall.enable = false;
  systemd.tmpfiles.rules = [
    "f /etc/munge/munge.key 0400 munge munge - mungeverryweakkeybuteasytointegratoinatest"
  ];
}
