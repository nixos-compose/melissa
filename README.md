# nxc - melissaSA (sensitivity analysis)

This project is a multi-compositions :
- melissaSA + slurm
- melissaSA + OAR3

Goal, being able to quickly confront the melissa framework against differents RJMS, locally during development or on g5k for testing purposes.

# melissaSA

The framework has been packaged in nur-kapack. You'll find the `melissa` package that include the framework, the `melissa-launcher`, the `melissa-server` and `melissa-config` binaries.

The example `heat-pde` is also packaged in nur-kapack.

TODO :
- the development effort is now on `melissa-deep` and a new version of the launcher is also developed. This means that this new melissa version needs to be Nix packaged. The usage of pytorch in `melissa-deep` allows for simulation to work with cpu or gpu resources.

Last Marc's message

Pour deep-melissa sur g5k tu peux regarder la branche 19-oar-heat-pde.
Malheureusement la doc pour l'installation est pas à jour mais y en a une là: [issue #16](https://gitlab.inria.fr/melissa/deep-melissa/-/issues/16)

Si tu suis cette doc tu peux skipper la partie "A first execution" qui explique comment passer de Jean-Zay (slurm) à G5K (OAR) puisque la branche 19-oar-heat-pde est déjà prête à l'emploi spécifiquement pour g5k


# Experiment scripts

The frontend of each composition as 2 scripts called `heatScript` and `plotScript`. The first one runs the simulation thanks to the `melissa-launcher` and the results are in `/data/melissa-results`, the second one formats the results to a mp4 video.

# melissaSA + Slurm composition

roles : [ dbd server frontend computeNodes ]

`server` node : runs also the NFS.

### localy with docker
[ tested and worked on 25 sept ]

```
nix develop
nxc build -C slurm::docker
nxc start -C slurm::docker nodes.yaml
nxc connect frontend
```

On frontend :

```
heatScript
plotScript
```

### g5k (build command on a build node)
[ tested and worked on 25 sept 2022 on dahu ]

```
nxc build -C slurm::g5k-nfs-store
export $(oarsub -p dahu -l nodes=13,walltime=0:30 "$(nxc helper g5k_script) 30m" | grep OAR_JOB_ID)
nxc start -C slurm::g5k-nfs-store -m OAR.$OAR_JOB_ID.stdout nodes.yaml
nxc connect frontend
```

On frontend :
```
heatScript
plotScript
```


### To solve/TODO

- the number of compute nodes is arbitrarly set to 10, Christoph (former melissa engineer) saw me having issue with 2 computeNode and told me to use 10. He also told me that the ressource usage was not optimal with the `sbatch` generated.
- the slurm configuration is far from optimal, right now it uses 1 CPU per node (in Slurm terms CPU = core). Solution not tested :
    - write a correct conf that targets only one cluster and always reserves the same homogene cluster
    - (or) write a oneshot service that generates the configuration file (see the option `etcSlurm` as a candidate)

# melissaSA + OAR3

roles = [ server frontend node ]

`server` node runs also the NFS

### locally

```
nix develop
nxc build -C oar::docker -s dev
nxc start -C oar::docker
nxc connect frontend
```

### g5k (build command on a build node)
```
nxc build -C oar::g5k-nfs-store -s dev
export $(oarsub -p dahu -l nodes=12,walltime=0:30 "$(nxc helper g5k_script) 30m" | grep OAR_JOB_ID)
nxc start -C oar::g5k-nfs-store -m OAR.$OAR_JOB_ID.stdout
nxc connect frontend
```

On frontend ;
```
su user1
heatScript
plotScript
```

### To solve/TODO

#### docker flavour

The docker flavour is very slow and waits indefinitly for the 6/6 simulation.
```
cat /data/melissa-results/melissa-server.log
```

#### g5k-nfs-store

- The g5k-nfs-store flavour encounter an issue on the server. The `oar-db-init` service failed. Can't figure out what is the issue, needs to cross source with Adrien Faure who has a similar composition only for OAR testing and it works.
- adding auto registering from the nodes to cover heterogeneous cluster.
- the nfs does not seems to work anymore if on frontend as user1.
