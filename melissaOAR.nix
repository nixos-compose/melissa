{ pkgs, modulesPath, nur, helpers, flavour, ... }: {
  nodes = let
    nfsConfigs = import ./nfs.nix { inherit flavour; };
    commonConfig =
      import ./oar/common_config.nix { inherit pkgs modulesPath nur; };
    melissaConf = import ./melissa/melissa-conf.nix { inherit pkgs; };

    node = { ... }: {
      imports = [ commonConfig nfsConfigs.client melissaConf.oar ];
      services.oar.node = { enable = true; };
    };

  in {

    frontend = { ... }: {
      imports = [ commonConfig nfsConfigs.client melissaConf.oar ];
      services.oar.client.enable = true;
    };

    server = { ... }: {
      imports = [ commonConfig nfsConfigs.server ];

      systemd.services.oar-post-init = {
        after = [ "oar-db-init.service" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig.Type = "oneshot";
        script = ''
          num_nodes=$(cat /etc/num_nodes)
          num_cores=$(cat /etc/num_cores)

          ${pkgs.nur.repos.kapack.oar}/bin/.oarproperty -a cpu || true
          ${pkgs.nur.repos.kapack.oar}/bin/.oarproperty -a core || true

          totalcores=$((num_nodes*num_cores))
          cpu=0
          for ((core=1;core<=$totalcores; core++)); do
            if (((core-1)%num_cores==0)); then
                cpu=$((cpu + 1))
            fi

            ${pkgs.nur.repos.kapack.oar}/bin/.oarnodesetting -r $core -p cpu=$cpu -p core=$core &
            wait
          done
        '';
      };
      services.oar.server.enable = true;
      services.oar.dbserver.enable = true;
    };
  } // helpers.makeMany node "node" 10;
  testScript = ''
    frontend.succeed("true")
  '';
}
