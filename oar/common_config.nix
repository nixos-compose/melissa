{ pkgs, modulesPath, nur }:
let
  mpitest = import ../mpitest/mpitest.nix { inherit pkgs; };

  inherit (import "${toString modulesPath}/tests/ssh-keys.nix" pkgs)
    snakeOilPrivateKey snakeOilPublicKey;

  add_resources = pkgs.writers.writePython3Bin "add_resources" {
    libraries = [ pkgs.nur.repos.kapack.oar ];
  } ''
    from oar.lib.tools import get_date
    from oar.lib.resource_handling import resources_creation
    import sys
    import time
    r = True
    while r:
        try:
            print(get_date())  # date took from db (test connection)
            r = False
        except Exception:
            print("DB is not ready")
            time.sleep(0.25)

    resources_creation("node", int(sys.argv[1]), int(sys.argv[2]))
  '';
in {
  imports = [ mpitest nur.repos.kapack.modules.oar ];

  environment.systemPackages = [
    pkgs.python3
    pkgs.nur.repos.kapack.melissa
    pkgs.nur.repos.kapack.melissa-heat-pde
    pkgs.openmpi
  ];
  networking.firewall.enable = false;
  users.users.user1 = { isNormalUser = true; };
  users.users.user2 = { isNormalUser = true; };

  environment.etc."privkey.snakeoil" = {
    mode = "0600";
    source = snakeOilPrivateKey;
  };
  environment.etc."pubkey.snakeoil" = {
    mode = "0600";
    #source = snakeOilPublicKey;
    text = snakeOilPublicKey;
  };
  environment.etc."oar-dbpassword".text = ''
    # DataBase user name
    DB_BASE_LOGIN="oar"

    # DataBase user password
    DB_BASE_PASSWD="oar"

    # DataBase read only user name
    DB_BASE_LOGIN_RO="oar_ro"

    # DataBase read only user password
    DB_BASE_PASSWD_RO="oar_ro"
  '';
  services.oar = {
    extraConfig = {
      HIERARCHY_LABELS = "resource_id,network_address,cpu,core";
    };

    database = {
      host = "server";
      passwordFile = "/etc/oar-dbpassword";
      initPath = [ pkgs.util-linux pkgs.gawk pkgs.jq add_resources ];
      postInitCommands = ''
        num_cores=$(( $(lscpu | awk '/^Socket\(s\)/{ print $2 }') * $(lscpu | awk '/^Core\(s\) per socket/{ print $4 }') ))
        echo $num_cores > /etc/num_cores

        if [[ -f /etc/nxc/deployment-hosts ]]; then
          num_nodes=$(grep node /etc/nxc/deployment-hosts | wc -l)
        else
          num_nodes=$(jq -r '[.nodes[] | select(contains("node"))]| length' /etc/nxc/deployment.json)
        fi
        echo $num_nodes > /etc/num_nodes

        add_resources $num_nodes $num_cores
      '';
    };
    server.host = "server";
    privateKeyFile = "/etc/privkey.snakeoil";
    publicKeyFile = "/etc/pubkey.snakeoil";
  };

  users.users.root.password = "nixos";
  services.openssh.permitRootLogin = "yes";
}
