{
  description = "nixos-compose - melissa attempt";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/22.05";
    NUR.url = "github:nix-community/NUR";
    kapack.url = "github:oar-team/nur-kapack/master";
    kapack.inputs.nixpkgs.follows = "nixpkgs";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
  };

  outputs = { self, nixpkgs, NUR, kapack, nxc }:
    let system = "x86_64-linux";
    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system NUR;
        setup = ./setup.toml;
        repoOverrides = { inherit kapack; };
        compositions = ./compositions.nix;
      };

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;
    };
}
