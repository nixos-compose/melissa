{ lib, pkgs, flavour, ... }: {
  nodes = let
    nComputeNode = "10";
    nfsConfigs = import ./nfs.nix { inherit flavour; };
    dbdConfig = import ./slurm/slurmdbd.nix { inherit pkgs; };
    slurmConfig = import ./slurm/slurmconfig.nix { inherit pkgs nComputeNode; };
    melissaConf = import ./melissa/melissa-conf.nix { inherit pkgs; };
  in {
    dbd = { pkgs, ... }: {
      imports = [ dbdConfig ];
      systemd.services.slurmdbd.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    server = { pkgs, ... }: {
      imports = [ slurmConfig nfsConfigs.server ];
      services.slurm = { server.enable = true; };
      systemd.services.slurmctld.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    computeNode = { pkgs, ... }: {
      imports = [ slurmConfig nfsConfigs.client melissaConf.slurm ];
      services.slurm = { client.enable = true; };
      systemd.services.slurmd.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    frontend = { pkgs, ... }: {
      imports = [ slurmConfig nfsConfigs.client melissaConf.slurm ];
      services.slurm = { enableStools = true; };
    };

  };

  testScript = ''
    start_all()

    # Make sure DBD is up after DB initialzation
    with subtest("can_start_slurmdbd"):
        dbd.succeed("systemctl restart slurmdbd")
        dbd.wait_for_unit("slurmdbd.service")
        dbd.wait_for_open_port(6819)

    # there needs to be an entry for the current
    # cluster in the database before slurmctld is restarted
    with subtest("add_account"):
        server.succeed("sacctmgr -i add cluster default")
        # check for cluster entry
        server.succeed("sacctmgr list cluster | awk '{ print $1 }' | grep default")

    with subtest("can_start_slurmctld"):
        server.succeed("systemctl restart slurmctld")
        server.wait_for_unit("slurmctld.service")

    with subtest("can_start_slurmd"):
            node1.succeed("systemctl restart slurmd.service")
            node1.wait_for_unit("slurmd")

    # Some test remove due to variable amount of compute node
    # Test that the cluster works and can distribute jobs;

    # with subtest("run_distributed_command"):
    #     # Run `hostname` on 3 nodes of the partition (so on all the 3 nodes).
    #     # The output must contain the 3 different names
    #     frontend.succeed("srun -N 3 hostname | sort | uniq | wc -l | xargs test 3 -eq")

    #    with subtest("check_slurm_dbd"):
    #        # find the srun job from above in the database
    #        server.succeed("sleep 5")
    #        server.succeed("sacct | grep hostname")

    # with subtest("run_PMIx_mpitest"):
    #     frontend.succeed("srun -N 3 --mpi=pmix mpitest | grep size=3")

    with subtest("check if melissa binaries working"):
      frontend.succeed("melissa-config --version")
      frontend.succeed("melissa-server -h")
      frontend.succeed("melissa-launcher -h")

    with subtest("check filesharing")
      frontend.succeed("touch /data/testifle.txt")
  '';
}
