{ pkgs, ... }:
let
  heat-script-slurm = pkgs.writeScriptBin "heatScript" ''
    OPTIONS=''${1-${pkgs.nur.repos.kapack.melissa-heat-pde}/options.py}
    cd /data
    rm -rf melissa-results
    ${pkgs.nur.repos.kapack.melissa}/bin/melissa-launcher --output-dir='melissa-results' --scheduler-arg='--account=abc@cpu' slurm $OPTIONS heatc
  '';

  heat-script-oar = pkgs.writeScriptBin "heatScript" ''
    OPTIONS=''${1-${pkgs.nur.repos.kapack.melissa-heat-pde}/options.py}
    cd /data
    rm -rf melissa-results
    mkdir melissa-results
    ${pkgs.nur.repos.kapack.melissa}/bin/melissa-launcher --output-dir='melissa-results' --scheduler-arg='--project=pr-abc' oar $OPTIONS heatc
  '';

  my-python-packages = python-packages: with python-packages; [ matplotlib ];
  python-with-my-packages = pkgs.python3.withPackages my-python-packages;
  plot-script = pkgs.writeScriptBin "plotScript" ''
    PLOT=${pkgs.nur.repos.kapack.melissa-heat-pde}/plot-results.py
    cd /data
    ${python-with-my-packages}/bin/python3 $PLOT melissa-results temperature mean
  '';

in rec {
  slurm = {
    environment.systemPackages = [
      heat-script-slurm
      plot-script
      pkgs.ffmpeg
      pkgs.nur.repos.kapack.melissa
      pkgs.nur.repos.kapack.melissa-heat-pde
    ];
  };

  oar = {
    environment.systemPackages = [
      pkgs.ffmpeg
      heat-script-oar
      plot-script
      pkgs.nur.repos.kapack.melissa
      pkgs.nur.repos.kapack.melissa-heat-pde
      pkgs.openmpi
      pkgs.python3
    ];
  };
}
